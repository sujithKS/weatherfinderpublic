package com.mobiquity.challenge.mobiquityapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.mobiquity.challenge.mobiquityapp.adapters.MainAdapter
import com.mobiquity.challenge.mobiquityapp.data.Status
import com.mobiquity.challenge.mobiquityapp.data.User
import com.mobiquity.challenge.mobiquityapp.data.api.ApiHelper
import com.mobiquity.challenge.mobiquityapp.data.api.RetrofitBuilder
import com.mobiquity.challenge.mobiquityapp.databinding.ActivityFavLocListBinding
import com.mobiquity.challenge.mobiquityapp.ui.base.ViewModelFactory
import com.mobiquity.challenge.mobiquityapp.viewmodels.MainViewModel

class FavLocListActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       // setContentView(R.layout.activity_fav_loc_list)
        binding  = DataBindingUtil.setContentView(this, R.layout.activity_fav_loc_list)
        setupViewModel()
        setupUI()
        setupObservers()
    }
    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: MainAdapter
    lateinit var   binding:ActivityFavLocListBinding

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(ApiHelper(RetrofitBuilder.apiService))
        ).get(MainViewModel::class.java)
    }

    private fun setupUI() {
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = MainAdapter(arrayListOf())
        binding. recyclerView.addItemDecoration(
            DividerItemDecoration(
                binding.recyclerView.context,
                (binding.recyclerView.layoutManager as LinearLayoutManager).orientation
            )
        )
        binding.recyclerView.adapter = adapter
    }

    private fun setupObservers() {
        viewModel.getUsers().observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        binding.recyclerView.visibility = View.VISIBLE
                        binding.progressBar.visibility = View.GONE
                        resource.data?.let { users -> retrieveList(users) }
                    }
                    Status.ERROR -> {
                        binding.recyclerView.visibility = View.VISIBLE
                        binding. progressBar.visibility = View.GONE
                        Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> {
                        binding.progressBar.visibility = View.VISIBLE
                        binding. recyclerView.visibility = View.GONE
                    }
                }
            }
        })
    }

    private fun retrieveList(users: List<User>) {
        adapter.apply {
            addUsers(users)
            notifyDataSetChanged()
        }
    }
}