package com.mobiquity.challenge.mobiquityapp.data.repository

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.mobiquity.challenge.mobiquityapp.data.SavedLocation
@Dao
interface  LocationDao {
    @Query("SELECT * FROM savedlocation")
    fun getAll(): List<SavedLocation>

    @Query("SELECT * FROM savedlocation")
    fun loadAllLoc(): List<SavedLocation>

    @Query("SELECT * FROM savedlocation")
    fun findByName(): SavedLocation

    @Insert
    fun insertAll(vararg loc: SavedLocation)

/*    @Delete
    fun delete(savedLocation: SavedLocation)*/

}