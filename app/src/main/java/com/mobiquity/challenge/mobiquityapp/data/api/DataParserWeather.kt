package com.mobiquity.challenge.mobiquityapp.data.api

import com.mobiquity.challenge.mobiquityapp.data.Day
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class DataParserWeather  {
    // These are the names of the JSON objects that need to be extracted.

    companion object {
        private val OWM_LIST = "list"

        private val OWM_WEATHER = "weather"
        private val OWM_TEMPERATURE = "temp"
        private val OWM_MAX = "max"
        private val OWM_MIN = "min"
        private val OWM_DESCRIPTION = "main"
        private val OWM_ICON = "icon"
        private val OWM_HUMIDITY = "humidity"
        private val OWM_PRESSURE = "pressure"
        private val OWM_WIND = "speed"

        /**
         * Take the String representing the complete forecast in JSON Format and
         * pull out the data we need to construct the Strings needed for the wireframes.
         *
         * Fortunately parsing is easy: constructor takes the JSON string and converts it
         * into an Object hierarchy for us.
         */

        @Throws(JSONException::class)
        fun getWeatherDataFromJson(forecastJsonStr: String?): ArrayList<Day>? {
            val forecastJson = JSONObject(forecastJsonStr)
            val weatherArray = forecastJson.getJSONArray(OWM_LIST)

            // OWM returns daily forecasts based upon the local time of the city that is being
            // asked for, which means that we need to know the GMT offset to translate this data
            // properly.

            // Since this data is also sent in-order and the first day is always the
            // current day, we're going to take advantage of that to get a nice
            // normalized UTC date for all of our weather.
            val forecastDays: ArrayList<Day> = ArrayList<Day>()
            for (i in 0 until weatherArray.length()) {
                // Get the JSON object representing the day
                val dayForecast = weatherArray.getJSONObject(i)
                val gc = GregorianCalendar()
                gc.add(Calendar.DAY_OF_MONTH, i)

                // description is in a child array called "weather", which is 1 element long.
                val weatherObject = dayForecast.getJSONArray(OWM_WEATHER).getJSONObject(0)
                val description = weatherObject.getString(OWM_DESCRIPTION)
                val icon = weatherObject.getString(OWM_ICON)
                val dayForecast1 = dayForecast.getJSONObject("main")
                val humidity = dayForecast1.getDouble(OWM_HUMIDITY)
                val pressure = dayForecast1.getDouble(OWM_PRESSURE)
                val windObj = dayForecast.getJSONObject("wind")
                val wind = windObj.getDouble(OWM_WIND)

                // Temperatures are in a child object called "temp".  Try not to name variables
                // "temp" when working with temperature.  It confuses everybody.
              //  val temperatureObject = dayForecast.getJSONObject(OWM_TEMPERATURE)
                val currDay = Day()
                currDay.setDateInMillis(gc.timeInMillis)
                currDay.setTempMax(dayForecast1.getDouble("temp_max"))
                currDay.setTempMin(dayForecast1.getDouble("temp_min"))
                currDay.setWeatherDescription(description)
                currDay.setIconCode(icon)
                currDay.setHumidity(humidity)
                currDay.setPressure(pressure)
                currDay.setWind(wind)
                forecastDays.add(currDay)
            }
            return forecastDays
        }
    }
}