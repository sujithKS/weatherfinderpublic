package com.mobiquity.challenge.mobiquityapp.ui.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mobiquity.challenge.mobiquityapp.data.api.ApiHelper
import com.mobiquity.challenge.mobiquityapp.data.repository.MainRepository
import com.mobiquity.challenge.mobiquityapp.viewmodels.MainViewModel
import com.mobiquity.challenge.mobiquityapp.viewmodels.WeatherViewModel

class ViewModelFactory(private val apiHelper: ApiHelper) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(MainRepository(apiHelper)) as T
        }else if (modelClass.isAssignableFrom(WeatherViewModel::class.java)) {
            return WeatherViewModel(MainRepository(apiHelper)) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }

}