package com.mobiquity.challenge.mobiquityapp.adapters

import android.text.method.TextKeyListener.clear
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mobiquity.challenge.mobiquityapp.R
import com.mobiquity.challenge.mobiquityapp.callback.AddressClickCallback
import com.mobiquity.challenge.mobiquityapp.data.SavedLocation
import com.mobiquity.challenge.mobiquityapp.data.User
import com.mobiquity.challenge.mobiquityapp.databinding.FavLocItemLayoutBinding
import java.util.Collections.addAll

class FavLocAdapter(private val savedLocation: ArrayList<SavedLocation>,private val addressClickCallback: AddressClickCallback) : RecyclerView.Adapter<FavLocAdapter.DataViewHolder>() {

/*    class DataViewHolder(binding: FavLocItemLayoutBinding) : RecyclerView.ViewHolder(binding.root) {

        holder.binding.textViewUserName.text = user.name
        textViewUserEmail.text = user.email
        Glide.with(imageViewAvatar.context)
        .load(user.avatar)
        .into(imageViewAvatar)
        }*/
    class DataViewHolder(val binding: FavLocItemLayoutBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavLocAdapter.DataViewHolder {
        val binding = DataBindingUtil.inflate<FavLocItemLayoutBinding>(LayoutInflater.from(parent.context), R.layout.fav_loc_item_layout,
            parent, false)


        return DataViewHolder(binding)
    }

    override fun getItemCount(): Int = savedLocation.size
    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        val savedLoc = savedLocation.get(position)
        holder.binding.textViewPlace.text = savedLoc.name+"\nLatitude: "+savedLoc.lat+"\nLongitude: "+savedLoc.lon
        holder.binding.textViewaddress.text = savedLoc.address
        holder.itemView.setOnClickListener { addressClickCallback.onClickAddress(holder.itemView,savedLoc) }
       /* Glide.with(holder.binding.imageViewAvatar.context)
            .load(user.avatar)
            .into(holder.binding.imageViewAvatar)*/
    }
    fun addsavedLocation(savedLocat: ArrayList<SavedLocation>) {
        this.savedLocation.apply {
            clear()
            addAll(savedLocat)
        }
        notifyDataSetChanged()

    }
    }



