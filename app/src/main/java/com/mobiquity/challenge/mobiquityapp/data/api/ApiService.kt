package com.mobiquity.challenge.mobiquityapp.data.api

import retrofit2.Call
import retrofit2.http.*


interface ApiService {


    @FormUrlEncoded
    @POST("weather")
    suspend fun getweather(@Field("lat") lat: String, @Field("lon") long: String, @Field("APPID") appid: String): String

    @GET("data/2.5/forecast?")
    suspend fun getCurrentWeatherData(
        @Query("lat") lat: String?,
        @Query("lon") lon: String?,
        @Query("APPID") app_id: String?
    ): String?
    @GET("data/2.5/weather?")
    suspend fun getCurrentWeatherDataOneday(
        @Query("lat") lat: String?,
        @Query("lon") lon: String?,
        @Query("APPID") app_id: String?
    ): String?
}