package com.mobiquity.challenge.mobiquityapp.data

import android.os.Parcel
import android.os.Parcelable
import java.util.*

class Day() :Parcelable {
    private var dateInMillis: Long = 0
    private var tempMax = 0.0
    private var tempMin = 0.0
    private var weatherDescription: String? = null
    private var iconCode: String? = null
    private var humidity = 0.0
    private  var pressure = 0.0
    private  var wind = 0.0
    private var windDirection: String? = null

    fun Day() {}

    fun Day(`in`: Parcel): Day? {
        dateInMillis = `in`.readLong()
        tempMax = `in`.readDouble()
        tempMin = `in`.readDouble()
        weatherDescription = `in`.readString()
        //        this.tempCelcius = Util.convertFahrenheitToCelcius(tempFahrenheit);
        iconCode = `in`.readString()
        humidity = `in`.readDouble()
        pressure = `in`.readDouble()
        wind = `in`.readDouble()
        windDirection = `in`.readString()
        return this
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeLong(dateInMillis)
        dest.writeDouble(tempMax)
        dest.writeDouble(tempMin)
        dest.writeString(weatherDescription)
        dest.writeString(iconCode)
        dest.writeDouble(humidity)
        dest.writeDouble(pressure)
        dest.writeDouble(wind)
        dest.writeString(windDirection)
    }

    val CREATOR: Parcelable.Creator<Day?> = object : Parcelable.Creator<Day?> {
        override fun createFromParcel(`in`: Parcel): Day? {
            return Day(`in`)
        }

        override fun newArray(size: Int): Array<Day?> {
            return arrayOfNulls(size)
        }
    }

    fun getDate(): Calendar? {
        val date = Calendar.getInstance()
        date.timeInMillis = dateInMillis
        return date
    }

    fun getDateInMillis(): Long {
        return dateInMillis
    }

    fun setDateInMillis(dateInMillis: Long) {
        this.dateInMillis = dateInMillis
    }

    fun getTempMax(): Double {
        return tempMax
    }

    fun setTempMax(tempMax: Double) {
        this.tempMax = tempMax
    }

    fun getTempMin(): Double {
        return tempMin
    }

    fun setTempMin(tempMin: Double) {
        this.tempMin = tempMin
    }

    fun getWeatherDescription(): String? {
        return weatherDescription
    }

    fun setWeatherDescription(weatherDescription: String?) {
        this.weatherDescription = weatherDescription
    }

    fun getIconCode(): String? {
        return iconCode
    }

    fun setIconCode(iconCode: String?) {
        this.iconCode = iconCode
    }

    fun getHumidity(): Double {
        return humidity
    }

    fun setHumidity(humidity: Double) {
        this.humidity = humidity
    }

    fun getPressure(): Double {
        return pressure
    }

    fun setPressure(pressure: Double) {
        this.pressure = pressure
    }

    fun getWind(): Double {
        return wind
    }

    fun setWind(wind: Double) {
        this.wind = wind
    }

    fun getWindDirection(): String? {
        return windDirection
    }

    fun setWindDirection(windDirection: String?) {
        this.windDirection = windDirection
    }
}