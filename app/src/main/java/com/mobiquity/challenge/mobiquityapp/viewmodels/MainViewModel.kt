package com.mobiquity.challenge.mobiquityapp.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.mobiquity.challenge.mobiquityapp.data.Resource
import com.mobiquity.challenge.mobiquityapp.data.repository.MainRepository
import kotlinx.coroutines.Dispatchers

class MainViewModel(private val mainRepository: MainRepository) : ViewModel() {

    fun getUsers() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            //emit(Resource.success(data = mainRepository.getUsers()))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }
}