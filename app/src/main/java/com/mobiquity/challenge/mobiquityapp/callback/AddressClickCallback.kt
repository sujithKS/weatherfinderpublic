package com.mobiquity.challenge.mobiquityapp.callback

import android.view.View
import com.mobiquity.challenge.mobiquityapp.data.SavedLocation

interface AddressClickCallback {
    fun onClickAddress(view: View, savedLocation: SavedLocation)

}