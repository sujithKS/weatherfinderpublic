package com.mobiquity.challenge.mobiquityapp.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "savedlocation")
data class SavedLocation(
        @PrimaryKey
    var id : Long? = 0,
    @ColumnInfo(name = "lat") var lat: String = "",
    @ColumnInfo(name   = "lon") var lon: String = "",
    @ColumnInfo(name = "name") var name: String = "",
    @ColumnInfo(name = "address") var address: String = ""


):Serializable

