package com.mobiquity.challenge.mobiquityapp.data

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}