package com.mobiquity.challenge.mobiquityapp.data

data class User(
    val avatar: String,
    val email: String,
    val id: String,
    val name: String
)