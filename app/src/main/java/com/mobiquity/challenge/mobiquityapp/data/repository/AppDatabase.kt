package com.mobiquity.challenge.mobiquityapp.data.repository

import androidx.room.Database
import androidx.room.RoomDatabase
import com.mobiquity.challenge.mobiquityapp.data.SavedLocation

@Database(entities = arrayOf(SavedLocation::class), version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun locDao(): LocationDao
}
