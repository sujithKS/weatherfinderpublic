package com.mobiquity.challenge.mobiquityapp.data.repository

import com.mobiquity.challenge.mobiquityapp.data.api.ApiHelper

class MainRepository(private val apiHelper: ApiHelper)  {
    suspend fun getWeather(lat: String, long: String, appid: String) = apiHelper.getWether(lat,long,appid)
}