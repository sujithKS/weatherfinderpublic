package com.mobiquity.challenge.mobiquityapp.view

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProviders
import com.mobiquity.challenge.mobiquityapp.R
import com.mobiquity.challenge.mobiquityapp.adapters.MainAdapter
import com.mobiquity.challenge.mobiquityapp.data.api.ApiHelper
import com.mobiquity.challenge.mobiquityapp.data.api.RetrofitBuilder
import com.mobiquity.challenge.mobiquityapp.databinding.HomeActivityBinding
import com.mobiquity.challenge.mobiquityapp.ui.base.ViewModelFactory
import com.mobiquity.challenge.mobiquityapp.viewmodels.MainViewModel
import com.novaesln.mycricketclub.ui.profile.TabContainerFragment

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       // setContentView(R.layout.activity_fav_loc_list)
        binding  = DataBindingUtil.setContentView(this, R.layout.home_activity)
        setUpUI()
    }
    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: MainAdapter
    lateinit var   binding:HomeActivityBinding
    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(ApiHelper(RetrofitBuilder.apiService))
        ).get(MainViewModel::class.java)
    }
    var active: Fragment? = null;

    fun setUpUI()
    {
     var tabsContainerFag= TabContainerFragment.newInstance()
        addFragment(tabsContainerFag)
    }
    fun showHideFragment(fragment: Fragment) {
        if(active != null)
        {
            var fm = getSupportFragmentManager();
            fm.beginTransaction().hide(active!!).show(fragment).commit();
        }

    }
    fun addFragment(fragment: Fragment) {

        supportFragmentManager
                .beginTransaction()
                /*.setCustomAnimations(R.anim.design_bottom_sheet_slide_in, R.anim.design_bottom_sheet_slide_out)
               */ .replace(R.id.container, fragment, fragment.javaClass.getSimpleName())
                /* .addToBackStack(null)*/
                .commit()
        showHideFragment(fragment)
        active = fragment;
    }

    override fun onBackPressed() {
        if(active != null) {
            if (active is DetailActivityFragment) {

                var tabsContainerFag= TabContainerFragment.newInstance()
                addFragment(tabsContainerFag)
                return
            }
        }

        super.onBackPressed()
    }

}