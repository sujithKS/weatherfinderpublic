package com.mobiquity.challenge.mobiquityapp.adapters

import android.preference.PreferenceManager
import android.text.method.TextKeyListener.clear
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mobiquity.challenge.mobiquityapp.R
import com.mobiquity.challenge.mobiquityapp.callback.AddressClickCallback
import com.mobiquity.challenge.mobiquityapp.data.Day
import com.mobiquity.challenge.mobiquityapp.data.api.DataParserWeather
import com.mobiquity.challenge.mobiquityapp.data.repository.Util
import com.mobiquity.challenge.mobiquityapp.databinding.WeatherForcastItemLayoutBinding
import java.util.*
import kotlin.collections.ArrayList

class ForcastAdapter(private val dayItem: ArrayList<Day>) : RecyclerView.Adapter<ForcastAdapter.DataViewHolder>() {

/*    class DataViewHolder(binding: FavLocItemLayoutBinding) : RecyclerView.ViewHolder(binding.root) {

        holder.binding.textViewUserName.text = user.name
        textViewUserEmail.text = user.email
        Glide.with(imageViewAvatar.context)
        .load(user.avatar)
        .into(imageViewAvatar)
        }*/
    class DataViewHolder(val binding: WeatherForcastItemLayoutBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForcastAdapter.DataViewHolder {
        val binding = DataBindingUtil.inflate<WeatherForcastItemLayoutBinding>(LayoutInflater.from(parent.context), R.layout.weather_forcast_item_layout,
            parent, false)


        return DataViewHolder(binding)
    }

    override fun getItemCount(): Int = dayItem.size
    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        val selectedDay = dayItem.get(position)
        val today: Calendar = Calendar.getInstance()
        today.set(Calendar.HOUR_OF_DAY, 0)
        today.set(Calendar.MINUTE, 0)
        today.set(Calendar.SECOND, 0)
        today.set(Calendar.MILLISECOND, 0)
        val tomorrow: Calendar = Calendar.getInstance()
        tomorrow.add(Calendar.DAY_OF_YEAR, 1)
        tomorrow.set(Calendar.HOUR_OF_DAY, 0)
        tomorrow.set(Calendar.MINUTE, 0)
        tomorrow.set(Calendar.SECOND, 0)
        tomorrow.set(Calendar.MILLISECOND, 0)
        val selectedCalDate: Calendar = selectedDay.getDate()!!
        selectedCalDate.set(Calendar.HOUR_OF_DAY, 0)
        selectedCalDate.set(Calendar.MINUTE, 0)
        selectedCalDate.set(Calendar.SECOND, 0)
        selectedCalDate.set(Calendar.MILLISECOND, 0)


        // Day and Date
        val dayText =holder.binding.detailDayTextview
        dayText.setText(
                selectedDay.getDate()!!
                        .getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault()))


        // Handle Imperial vs Metric preference
        val maxTemp =holder.binding.detailHighTextview

        val minTemp =holder.binding.detailLowTextview

        maxTemp.text = java.lang.String.format(
                "%.0f",
                Util.convertFahrenheitToCelcius(selectedDay.getTempMax())
        ) + "\u00B0"
        minTemp.text = java.lang.String.format(
                "%.0f",
                Util.convertFahrenheitToCelcius(selectedDay.getTempMin())
        ) + "\u00B0"
      /* else {
            maxTemp.text = java.lang.String.format(
                    "%.0f",
                    selectedDay.getTempMax()
            ) + "\u00B0"
            minTemp.text = java.lang.String.format(
                    "%.0f",
                    selectedDay.getTempMin()
            ) + "\u00B0"
        }*/

        // Weather image
        val image: ImageView = holder.binding.detailIcon
        image.setImageResource(Util.getFeaturedWeatherIcon(selectedDay.getIconCode()))
        val weatherDescr =holder.binding.detailForecastTextview
        weatherDescr.setText(selectedDay.getWeatherDescription())

        // Humidity
        val humidityText =holder.binding.detailHumidityTextview
        humidityText.text =
                "Humidity" + ": " + java.lang.String.format(
                        "%.0f",
                        selectedDay.getHumidity()
                ) + " %"


        // Pressure
        val pressureText =holder.binding.detailPressureTextview
        pressureText.text =
                "Pressure" + ": " + java.lang.String.format(
                        "%.0f",
                        selectedDay.getPressure()
                ) + " hPa"

        // Wind
        val windText =holder.binding.detailWindTextview
        windText.text = "Wind" + ": " + java.lang.String.format(
                "%.0f",
                selectedDay.getWind()
        ) + " km/h "
       /* Glide.with(holder.binding.imageViewAvatar.context)
            .load(user.avatar)
            .into(holder.binding.imageViewAvatar)*/
    }
    fun adddayItem(savedLocat: ArrayList<Day>) {
        this.dayItem.apply {
            clear()
            addAll(savedLocat)
        }
        notifyDataSetChanged()

    }
    }



