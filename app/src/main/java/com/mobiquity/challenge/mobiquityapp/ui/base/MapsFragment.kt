package com.mobiquity.challenge.mobiquityapp.ui.base

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.mobiquity.challenge.mobiquityapp.R
import com.mobiquity.challenge.mobiquityapp.adapters.MainAdapter
import com.mobiquity.challenge.mobiquityapp.data.SavedLocation
import com.mobiquity.challenge.mobiquityapp.data.Status
import com.mobiquity.challenge.mobiquityapp.data.api.ApiHelper
import com.mobiquity.challenge.mobiquityapp.data.api.RetrofitBuilder
import com.mobiquity.challenge.mobiquityapp.databinding.HomeActivityBinding
import com.mobiquity.challenge.mobiquityapp.view.DetailActivityFragment
import com.mobiquity.challenge.mobiquityapp.view.HomeActivity
import com.mobiquity.challenge.mobiquityapp.viewmodels.WeatherViewModel
import java.util.*


class MapsFragment : Fragment() , OnMapReadyCallback, GoogleMap.OnMapClickListener{
    private lateinit var mMap: GoogleMap

/*    private val callback = OnMapReadyCallback { googleMap ->
        *//**
         * Manipulates the map once available.
         * This callback is triggered when the map is ready to be used.
         * This is where we can add markers or lines, add listeners or move the camera.
         * In this case, we just add a marker near Sydney, Australia.
         * If Google Play services is not installed on the device, the user will be prompted to
         * install it inside the SupportMapFragment. This method will only be triggered once the
         * user has installed Google Play services and returned to the app.
         *//*
        val sydney = LatLng(-34.0, 151.0)
        googleMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
    }*/
    companion object {
        fun newInstance():MapsFragment
        {
            val f = MapsFragment()

            return f
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
        setupViewModel()
    }
    override fun onMapClick(point: LatLng?) {
        val imageBitmap = BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(activity!!.getResources().getResourceName(R.drawable.pin1), "drawable", activity!!.getPackageName()))
        val resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, 38, 38, false);

       var savedLocation =  getAddress(point!!.latitude, point!!.longitude)
        mMap.addMarker(MarkerOptions()
            .position(LatLng(point?.latitude!!, point?.longitude!!))
            .anchor(0.5f, 0.1f)
            .title("")
            .snippet("")
            .icon(BitmapDescriptorFactory.fromBitmap(resizedBitmap)))

        Toast.makeText(activity,"Lati" +point.latitude + "  Long" + point.longitude, Toast.LENGTH_LONG).show()
         setupObservers(point.latitude.toString(), point.longitude.toString(),"d2b8e42cf44a74e6846ddeabf0a09140",savedLocation)
    }
    private fun setupObservers(lat:String, long: String, appid: String,savedLocation:SavedLocation) {
        viewModel.getwether(lat,long,appid).observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {

                        resource.data?.let { weather -> retrieveList(weather,savedLocation)}
                    }
                    Status.ERROR -> {

                        Toast.makeText(activity, it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> {

                    }
                }
            }
        })
    }
    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0!!

        val sydney = LatLng(-34.0, 151.0)
        mMap.addMarker(MarkerOptions()
            .position(sydney)
            .title("Marker in Sydney"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
        mMap.setOnMapClickListener(this)
    }
    private lateinit var viewModel: WeatherViewModel
    private lateinit var adapter: MainAdapter
    lateinit var   binding: HomeActivityBinding
    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(
                this,
                ViewModelFactory(ApiHelper(RetrofitBuilder.apiService))
        ).get(WeatherViewModel::class.java)
    }
    private fun retrieveList(data:String,savedLocation:SavedLocation) {
        Log.v("data",data)
        var detailActivityFragment = DetailActivityFragment.newInstance(data,savedLocation)
        var homeActivity = activity as HomeActivity
        homeActivity.addFragment(detailActivityFragment)

    }
    fun getAddress(latitude:Double,longitude:Double):SavedLocation

    {
        val geocoder: Geocoder
        val addresses: List<Address>
        val oneTimeID = SystemClock.uptimeMillis()

        geocoder = Geocoder(activity, Locale.getDefault())
        var savedLocation : SavedLocation = SavedLocation(oneTimeID,"","","","")
            try {
            addresses = geocoder.getFromLocation(
                latitude,
                longitude,
                1
            ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            val address: String = addresses[0]
                .getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

            var city: String = ""
            try {
                city = addresses[0].getPostalCode()
            }
            catch (ex:Exception)
            {

            }
            var state: String = ""
            try {
                state = addresses[0].getPostalCode()
            }
            catch (ex:Exception)
            {

            }
            var country: String = ""
            try {
                country = addresses[0].getPostalCode()
            }
            catch (ex:Exception)
            {

            }
            var postalCode: String = ""
            try {
                postalCode = addresses[0].getPostalCode()
            }
            catch (ex:Exception)
            {

            }
            val knownName: String =
                addresses[0].getFeatureName() // Only if available else return NULL
            var addressNew = city+", "+state+" \n"+postalCode+", "+knownName+" \n"+address
                val oneTimeID = SystemClock.uptimeMillis()
                var savedLocation  = SavedLocation(oneTimeID,latitude.toString(),longitude.toString(),country,addressNew);

            return savedLocation;
        }
        catch (ex:Exception)
        {

        }

        return savedLocation;

    }

}