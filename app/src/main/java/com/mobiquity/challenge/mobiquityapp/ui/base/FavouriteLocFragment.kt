package com.mobiquity.challenge.mobiquityapp.ui.base

import androidx.fragment.app.Fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room

import com.mobiquity.challenge.mobiquityapp.R
import com.mobiquity.challenge.mobiquityapp.adapters.FavLocAdapter
import com.mobiquity.challenge.mobiquityapp.adapters.MainAdapter
import com.mobiquity.challenge.mobiquityapp.callback.AddressClickCallback
import com.mobiquity.challenge.mobiquityapp.data.SavedLocation
import com.mobiquity.challenge.mobiquityapp.data.Status
import com.mobiquity.challenge.mobiquityapp.data.api.ApiHelper
import com.mobiquity.challenge.mobiquityapp.data.api.RetrofitBuilder
import com.mobiquity.challenge.mobiquityapp.data.repository.AppDatabase
import com.mobiquity.challenge.mobiquityapp.databinding.HomeActivityBinding
import com.mobiquity.challenge.mobiquityapp.view.DetailActivityFragment
import com.mobiquity.challenge.mobiquityapp.view.HomeActivity
import com.mobiquity.challenge.mobiquityapp.viewmodels.WeatherViewModel
import java.lang.Exception

class FavouriteLocFragment : Fragment(),AddressClickCallback {

/*    private val callback = OnMapReadyCallback { googleMap ->
        *//**
         * Manipulates the map once available.
         * This callback is triggered when the map is ready to be used.
         * This is where we can add markers or lines, add listeners or move the camera.
         * In this case, we just add a marker near Sydney, Australia.
         * If Google Play services is not installed on the device, the user will be prompted to
         * install it inside the SupportMapFragment. This method will only be triggered once the
         * user has installed Google Play services and returned to the app.
         *//*
        val sydney = LatLng(-34.0, 151.0)
        googleMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
    }*/
    companion object {
        fun newInstance():FavouriteLocFragment
        {
            val f = FavouriteLocFragment()

            return f
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_favourite_loc, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViewModel()
        setupUI(view.findViewById(R.id.recyclerView))
    }

    private fun setupObservers(lat:String, long: String, appid: String) {
        viewModel.getwether(lat,long,appid).observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {

                        resource.data?.let { weather -> retrieveList(weather)}
                    }
                    Status.ERROR -> {

                        Toast.makeText(activity, it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> {

                    }
                }
            }
        })
    }

    private lateinit var viewModel: WeatherViewModel
    private lateinit var adapter: FavLocAdapter
    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(
                this,
                ViewModelFactory(ApiHelper(RetrofitBuilder.apiService))
        ).get(WeatherViewModel::class.java)
    }
    private fun retrieveList(data:String) {
        Log.v("data",data)
        var detailActivityFragment = DetailActivityFragment.newInstance(data,null)
        var homeActivity = activity as HomeActivity
        homeActivity.addFragment(detailActivityFragment)

    }
    private fun setupUI(recyclerView:RecyclerView) {
        recyclerView.layoutManager = LinearLayoutManager(activity)
        adapter = FavLocAdapter(arrayListOf(),this)
     recyclerView.addItemDecoration(
            DividerItemDecoration(
            recyclerView.context,
                (recyclerView.layoutManager as LinearLayoutManager).orientation
            )
        )
        recyclerView.adapter = adapter
        updateAdapter()
    }
    fun updateAdapter()
    {
        val db = Room.databaseBuilder(
            activity!!.applicationContext,
            AppDatabase::class.java, "savedlocations"
        ).build()
        Thread {

            try {
                var savedlist = db.locDao().getAll()
                adapter.addsavedLocation(savedlist as ArrayList<SavedLocation>)
                Log.d("saved", savedlist.toString())
            }
            catch (ex:Exception)
            {

            }
        }.start()
    }

    override fun onClickAddress(view: View, savedLocation: SavedLocation) {
        setupObservers(savedLocation.lat,savedLocation.lon,"d2b8e42cf44a74e6846ddeabf0a09140")
    }

}