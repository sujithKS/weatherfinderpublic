package com.mobiquity.challenge.mobiquityapp.data.repository

import com.mobiquity.challenge.mobiquityapp.R

final class Util {
    companion object {
        fun convertFahrenheitToCelcius(degreesInFahrenheit: Double): Double {
            return (degreesInFahrenheit - 32) * 5 / 9
        }

        fun convertCelciusToFahrenheit(degreesInCelcius: Double): Double {
            return degreesInCelcius * 9 / 5 + 32
        }

        fun getFeaturedWeatherIcon(OWMIconCode: String?): Int {
            return when (OWMIconCode) {
                "01d" -> R.drawable.art_clear
                "01n" -> R.drawable.art_clear
                "02d", "02n" -> R.drawable.art_light_clouds
                "03d", "03n" -> R.drawable.art_clouds
                "04d", "04n" -> R.drawable.art_light_clouds
                "09d", "09n" -> R.drawable.art_rain
                "10d", "10n" -> R.drawable.art_light_rain
                "11d", "11n" -> R.drawable.art_storm
                "13d", "13n" -> R.drawable.art_snow
                "50d", "50n" -> R.drawable.art_fog
                else -> R.drawable.art_clear
            }
        }

        fun getItemWeatherIcon(OWMIconCode: String?): Int {
            return when (OWMIconCode) {
                "01d" -> R.drawable.ic_clear
                "01n" -> R.drawable.ic_clear
                "02d", "02n" -> R.drawable.ic_light_clouds
                "03d", "03n" -> R.drawable.ic_cloudy
                "04d", "04n" -> R.drawable.ic_light_clouds
                "09d", "09n" -> R.drawable.ic_rain
                "10d", "10n" -> R.drawable.ic_light_rain
                "11d", "11n" -> R.drawable.ic_storm
                "13d", "13n" -> R.drawable.ic_snow
                "50d", "50n" -> R.drawable.ic_fog
                else -> R.drawable.ic_clear
            }
        }
    }
}