package com.mobiquity.challenge.mobiquityapp.adapters

import android.text.method.TextKeyListener.clear
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mobiquity.challenge.mobiquityapp.R
import com.mobiquity.challenge.mobiquityapp.data.User
import com.mobiquity.challenge.mobiquityapp.databinding.ItemLayoutBinding
import java.util.Collections.addAll

class MainAdapter(private val users: ArrayList<User>) : RecyclerView.Adapter<MainAdapter.DataViewHolder>() {

/*    class DataViewHolder(binding: ItemLayoutBinding) : RecyclerView.ViewHolder(binding.root) {

        holder.binding.textViewUserName.text = user.name
        textViewUserEmail.text = user.email
        Glide.with(imageViewAvatar.context)
        .load(user.avatar)
        .into(imageViewAvatar)
        }*/
    class DataViewHolder(val binding: ItemLayoutBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainAdapter.DataViewHolder {
        val binding = DataBindingUtil.inflate<ItemLayoutBinding>(LayoutInflater.from(parent.context), R.layout.item_layout,
            parent, false)


        return DataViewHolder(binding)
    }

    override fun getItemCount(): Int = users.size
    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        val user = users.get(position)
        holder.binding.textViewUserName.text = user.name
        holder.binding.textViewUserEmail.text = user.email
        Glide.with(holder.binding.imageViewAvatar.context)
            .load(user.avatar)
            .into(holder.binding.imageViewAvatar)
    }
    fun addUsers(users: List<User>) {
        this.users.apply {
            clear()
            addAll(users)
        }

    }
    }



