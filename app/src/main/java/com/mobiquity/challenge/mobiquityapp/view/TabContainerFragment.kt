package com.novaesln.mycricketclub.ui.profile

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

import androidx.lifecycle.ViewModelProviders
import com.mobiquity.challenge.mobiquityapp.R
import com.mobiquity.challenge.mobiquityapp.databinding.FragmentTabsContainerBinding
import com.mobiquity.challenge.mobiquityapp.ui.base.FavouriteLocFragment
import com.mobiquity.challenge.mobiquityapp.ui.base.MapsFragment


/* 
 * Created by Sujith.
 */

class TabContainerFragment : Fragment() {


    lateinit var fragmentTabsContainerBinding: FragmentTabsContainerBinding
 lateinit var fromfrag: String

    companion object {
        fun newInstance() :TabContainerFragment

        {
            val f = TabContainerFragment ()
            // Pass index input as an argument.

            return f
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        fragmentTabsContainerBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_tabs_container, container, false
        )

         val view = fragmentTabsContainerBinding.getRoot()

        return view
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setStatePageAdapter()

    }

    private fun setStatePageAdapter(){
        try {
            val myViewPageStateAdapter = MyViewPageStateAdapter(this!!.fragmentManager!!);


                myViewPageStateAdapter.addFragment(
                    MapsFragment.newInstance(
                    ), "CHOOSE LOCATION"
                )
                    myViewPageStateAdapter.addFragment(
                    FavouriteLocFragment.newInstance(
                    ), "FAV LOCATIONS"
                )

            fragmentTabsContainerBinding.pager.adapter = myViewPageStateAdapter
            fragmentTabsContainerBinding.tabs.setupWithViewPager(
                fragmentTabsContainerBinding.pager,
                true
            )
        }
        catch (ex:Exception)
        {

        }
    }

    class MyViewPageStateAdapter(fm: FragmentManager): FragmentStatePagerAdapter(fm, FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT){
        val fragmentList:MutableList<Fragment> = ArrayList<Fragment>()
        val fragmentTitleList:MutableList<String> = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return fragmentList.get(position)
        }

        override fun getCount(): Int {
            return fragmentList.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return fragmentTitleList.get(position)
        }

        fun addFragment(fragment:Fragment,title:String){
            fragmentList.add(fragment)
            fragmentTitleList.add(title)

        }
    }
}
