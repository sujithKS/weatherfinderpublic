package com.mobiquity.challenge.mobiquityapp.view

import android.os.Bundle
import android.os.FileObserver
import android.preference.PreferenceManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.mobiquity.challenge.mobiquityapp.R
import com.mobiquity.challenge.mobiquityapp.adapters.FavLocAdapter
import com.mobiquity.challenge.mobiquityapp.adapters.ForcastAdapter
import com.mobiquity.challenge.mobiquityapp.data.Day
import com.mobiquity.challenge.mobiquityapp.data.SavedLocation
import com.mobiquity.challenge.mobiquityapp.data.api.DataParserWeather
import com.mobiquity.challenge.mobiquityapp.data.repository.AppDatabase
import com.mobiquity.challenge.mobiquityapp.data.repository.Util
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList


class DetailActivityFragment :Fragment(){
 var  savedLocation: SavedLocation? = null
 lateinit var  daysList: ArrayList<Day>

    companion object {
        fun newInstance(
            weatherJson:String,
            savedLocation: SavedLocation?
        ): DetailActivityFragment
        {
            val f = DetailActivityFragment()
            val args = Bundle()
            args.putString("weatherJson", weatherJson)
            args.putSerializable("savedLocation", savedLocation)

            f.setArguments(args)
            return f
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        // Attempt to restore from savedInstanceState
       /* if (savedInstanceState != null) {
            selectedDay = savedInstanceState.getParcelable<Parcelable>(SAVED_DAY)
        }*/
        try {
            savedLocation = arguments!!.getSerializable("savedLocation") as SavedLocation
        }
        catch (ex:Exception)
        {}

        daysList= DataParserWeather.getWeatherDataFromJson(arguments?.getString("weatherJson"))!!
        var selectedDay = daysList!!.get(0)
        val detailFragment: View = inflater.inflate(R.layout.fragment_detail, container, false)
            val today: Calendar = Calendar.getInstance()
            today.set(Calendar.HOUR_OF_DAY, 0)
            today.set(Calendar.MINUTE, 0)
            today.set(Calendar.SECOND, 0)
            today.set(Calendar.MILLISECOND, 0)
            val tomorrow: Calendar = Calendar.getInstance()
            tomorrow.add(Calendar.DAY_OF_YEAR, 1)
            tomorrow.set(Calendar.HOUR_OF_DAY, 0)
            tomorrow.set(Calendar.MINUTE, 0)
            tomorrow.set(Calendar.SECOND, 0)
            tomorrow.set(Calendar.MILLISECOND, 0)
            val selectedCalDate: Calendar = selectedDay.getDate()!!
            selectedCalDate.set(Calendar.HOUR_OF_DAY, 0)
            selectedCalDate.set(Calendar.MINUTE, 0)
            selectedCalDate.set(Calendar.SECOND, 0)
            selectedCalDate.set(Calendar.MILLISECOND, 0)

            // Day and Date
            val dayText =
                detailFragment.findViewById(R.id.detail_day_textview) as TextView
            if (selectedCalDate.equals(today)) {
                dayText.text = activity!!.applicationContext.getString(R.string.today)
            } else if (selectedCalDate.equals(tomorrow)) {
                dayText.text = activity!!.applicationContext.getString(R.string.tomorrow)
            } else {
                dayText.setText(
                    selectedDay.getDate()!!
                        .getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault())
                )
            }
            val dateText =
                detailFragment.findViewById(R.id.detail_date_textview) as TextView
            dateText.setText(
                "Today"
            )

            // Handle Imperial vs Metric preference
            val maxTemp =
                detailFragment.findViewById(R.id.detail_high_textview) as TextView
            val minTemp =
                detailFragment.findViewById(R.id.detail_low_textview) as TextView
            val sharedPrefs =
                PreferenceManager.getDefaultSharedPreferences(activity!!.applicationContext)
            val unitType = sharedPrefs.getString(
                activity!!.applicationContext.getString(R.string.pref_units_key),
                activity!!.applicationContext.getString(R.string.pref_units_metric)
            )
            if (unitType == activity!!.applicationContext
                    .getString(R.string.pref_units_metric)
            ) {
                maxTemp.text = java.lang.String.format(
                    "%.0f",
                    Util.convertFahrenheitToCelcius(selectedDay.getTempMax())
                ) + "\u00B0"
                minTemp.text = java.lang.String.format(
                    "%.0f",
                    Util.convertFahrenheitToCelcius(selectedDay.getTempMin())
                ) + "\u00B0"
            } else {
                maxTemp.text = java.lang.String.format(
                    "%.0f",
                    selectedDay.getTempMax()
                ) + "\u00B0"
                minTemp.text = java.lang.String.format(
                    "%.0f",
                    selectedDay.getTempMin()
                ) + "\u00B0"
            }

            // Weather image
            val image: ImageView = detailFragment.findViewById(R.id.detail_icon) as ImageView
            image.setImageResource(Util.getFeaturedWeatherIcon(selectedDay.getIconCode()))
            val weatherDescr =
                detailFragment.findViewById(R.id.detail_forecast_textview) as TextView
            weatherDescr.setText(selectedDay.getWeatherDescription())

            // Humidity
            val humidityText =
                detailFragment.findViewById(R.id.detail_humidity_textview) as TextView
            humidityText.text =
                getString(R.string.detail_humidity) + ": " + java.lang.String.format(
                    "%.0f",
                    selectedDay.getHumidity()
                ) + " %"


            // Pressure
            val pressureText =
                detailFragment.findViewById(R.id.detail_pressure_textview) as TextView
            pressureText.text =
                getString(R.string.detail_pressure) + ": " + java.lang.String.format(
                    "%.0f",
                    selectedDay.getPressure()
                ) + " hPa"

            // Wind
            val windText =
                detailFragment.findViewById(R.id.detail_wind_textview) as TextView
            windText.text = getString(R.string.detail_wind) + ": " + java.lang.String.format(
                "%.0f",
                selectedDay.getWind()
            ) + " km/h "
        val TV_markfav =  detailFragment.findViewById(R.id.TV_markfav) as TextView
        val recyclerView =  detailFragment.findViewById(R.id.recyclerView) as RecyclerView
        if(savedLocation == null){
            TV_markfav.visibility = GONE
        }
        setupUI(recyclerView!!)
        TV_markfav.setOnClickListener { saveLocation() }
        return detailFragment
    }

fun parseJson(stringJson: String) {

    var obj = JSONObject(stringJson)
    System.out.println("obj1: $obj")
    var sessionArray: JSONArray = obj.optJSONArray("weather")
    System.out.println("obj1: $sessionArray")
    var firstObject = sessionArray[0]
    System.out.println("obj1: $firstObject")
}
    fun saveLocation()
    {
        val db = Room.databaseBuilder(
            activity!!.applicationContext,
            AppDatabase::class.java, "savedlocations"
        ).build()
        Thread {
            db.locDao().insertAll(savedLocation!!)
            var savedlist =  db.locDao().getAll()
            Log.d("saved",savedlist.toString())
            activity!!.onBackPressed()
        }.start()



    }
lateinit var adapter:ForcastAdapter
    private fun setupUI(recyclerView: RecyclerView) {
        recyclerView.layoutManager = LinearLayoutManager(activity,LinearLayoutManager.HORIZONTAL,false)
        adapter = ForcastAdapter(arrayListOf())
        recyclerView.addItemDecoration(
                DividerItemDecoration(
                        recyclerView.context,
                        LinearLayoutManager.HORIZONTAL
                )
        )
        recyclerView.adapter = adapter
        updateAdapter(daysList)
    }
    fun updateAdapter(day: List<Day>)
    {

        Thread {

            try {

                adapter.adddayItem(day as ArrayList<Day>)

            }
            catch (ex:Exception)
            {

            }
        }.start()
    }
}