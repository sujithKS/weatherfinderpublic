package com.mobiquity.challenge.mobiquityapp.data.api

import com.mobiquity.challenge.mobiquityapp.data.api.RetrofitBuilder.apiService

class ApiHelper(apiService1: ApiService) {
    suspend fun getWether(lat: String, long: String, appid: String) = apiService.getCurrentWeatherData(lat,long,appid)
}